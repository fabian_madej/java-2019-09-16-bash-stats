#!/usr/bin/env bash
#1
echo `whoami`
echo `date +'%Y-%m-%d %H:%M'`
#2
echo `find | grep -P '.*.txt' | sort -r`
#3
echo `find | grep -P '.*.txt' | sort -r|wc -l`
#4
#?